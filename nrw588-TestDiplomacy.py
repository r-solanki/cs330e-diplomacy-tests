from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from io import StringIO

class TestDiplomacy(TestCase):

    def test_solve0(self):
        moves = StringIO('A Madrid Hold')
        result = 'A Madrid\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve1(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        result = 'A [dead]\nB Madrid\nC London\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve2(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid')
        result = 'A [dead]\nB [dead]\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve3(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        result = 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve4(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        result = 'A [dead]\nB [dead]\nC [dead]\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve5(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        result = 'A [dead]\nB Madrid\nC [dead]\nD Paris\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve6(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        result = 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n'
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())
    def test_solve7(self):
        moves = StringIO('')
        result = ''
        w = StringIO()
        x = diplomacy_solve(moves,w)
        self.assertEqual(result,w.getvalue())

if __name__ == "__main__":
    main()
