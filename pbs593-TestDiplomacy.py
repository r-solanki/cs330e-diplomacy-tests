

from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_in, evaluate_queue, output_res, solve



class TestDiplomacy(TestCase):


    def test_read_1(self):
        s = "A Berlin Hold"
        i = read_in(s)
        self.assertEqual(i,  ['A', 'Berlin', 'Hold', None])
    def test_read_2(self):
        s = "B Rome Move Berlin\n"
        i = read_in(s)
        self.assertEqual(i, ['B', 'Rome', 'Move', 'Berlin'])

    #----
    #eval
    #----

    def test_eval_1(self):
        game = [
                ['A', 'Berlin', 'Hold', None],
                ['B', 'Rome', 'Move', 'Berlin'],
                ['C', 'Miami', 'Support', 'B'],
            ]
        game_out = evaluate_queue(game)
        self.assertEqual(game_out, [['A', '[dead]'], ['B', 'Berlin'], ['C', 'Miami']])

    #-----
    #print
    #-----

    def test_print_1(self):
        w = StringIO()
        game_out = [['A', '[dead]'], ['B', 'Berlin'], ['C', 'Miami']]
        output_res(w, game_out)
        self.assertEqual(w.getvalue(), "A [dead]\nB Berlin\nC Miami\n")


    #-----
    #solve
    #-----

    def test_solve_1(self):
        r = StringIO("A Berlin Hold\nB Rome Move Berlin\nC Miami Support B\nD NewOrleans Move Miami\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Berlin Hold\nB Rome Move Berlin\nC Miami Move Berlin\nD NewOrleans Support B\nE Richmond Support A\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD NewOrleans\nE Richmond\n")

    def test_solve_3(self):
        r = StringIO("A Berlin Hold\nB Rome Move Berlin\nC Miami Move Berlin\nD NewOrleans Support B\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Berlin\nC [dead]\nD NewOrleans\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
