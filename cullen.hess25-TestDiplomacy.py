#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, diplomacy_eval

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A Madrid Hold"])

    def test_read_2(self):
        s = "A Madrid Move Barcelona\nB Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A Madrid Move Barcelona", "B Madrid Hold"])

    def test_read_3(self):
        s = "A Madrid Support B\nB Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A Madrid Support B", "B Madrid Hold"])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London', 'E Houston Support D'])
        self.assertEqual(v, {'A': 'dead', 'B': 'dead', 'C': 'dead', 'D': 'London', 'E': 'Houston'})

    def test_eval_2(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid'])
        self.assertEqual(v, {'A': 'dead', 'B': 'dead', 'C': 'dead'})

    def test_eval_3(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'])
        self.assertEqual(v, {'A': 'dead', 'B': 'dead', 'C': 'dead', 'D': 'Paris', 'E': 'Austin'})

    def test_eval_4(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B'])
        self.assertEqual(v, {'A': 'dead', 'B': 'Madrid', 'C': 'dead', 'D': 'Paris'})

    def test_eval_5(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London', 'E Houston Support C'])
        self.assertEqual(v, {'A': 'dead', 'B': 'Madrid', 'C': 'London', 'D': 'dead', 'E': 'Houston'})

    def test_eval_6(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London', 'E Houston Support C', "F Galveston Move Houston"])
        self.assertEqual(v, {'A': 'dead', 'B': 'dead', 'C': 'dead', 'D': 'dead', 'E': 'dead', 'F': 'dead'})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'dead', 'B': 'Madrid', 'C': 'London', 'D': 'dead', 'E': 'Houston'})
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD [dead]\nE Houston\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'dead', 'B': 'dead', 'C': 'dead'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'dead', 'B': 'dead', 'C': 'dead', 'D': 'dead', 'E': 'dead', 'F': 'dead'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'dead', 'B': 'dead', 'C': 'dead', 'D': 'Paris', 'E': 'Austin'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Houston Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD [dead]\nE Houston\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Houston Support C\nF Galveston Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""


