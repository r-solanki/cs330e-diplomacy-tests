# Samuel Crabb
# sac4728
# 11/10/21

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        commands = diplomacy_read(r)
        self.assertEqual(commands, [["A", "Madrid", "Hold"],
                                    ["B", "Barcelona", "Move", "Madrid"],
                                    ["C", "London", "Support", "B"]])

    def test_read_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        commands = diplomacy_read(r)
        self.assertEqual(commands, [["A", "Madrid", "Hold"],
                                    ["B", "Barcelona", "Move", "Madrid"],
                                    ["C", "London", "Support", "B"],
                                    ["D", "Austin", "Move", "London"]])

    def test_read_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
                     "D Paris Support B\nE Austin Support A\n")
        commands = diplomacy_read(r)
        self.assertEqual(commands, [["A", "Madrid", "Hold"],
                                    ["B", "Barcelona", "Move", "Madrid"],
                                    ["C", "London", "Move", "Madrid"],
                                    ["D", "Paris", "Support", "B"],
                                    ["E", "Austin", "Support", "A"]])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],
                            ["B", "Barcelona", "Move", "Madrid"],
                            ["C", "London", "Move", "Madrid"],
                            ["D", "Paris", "Support", "B"],
                            ["E", "Dublin", "Support", "D"]])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "Madrid",
                             "C": "[dead]",
                             "D": "Paris",
                             "E": "Dublin"})

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],
                            ["B", "Barcelona", "Move", "Madrid"],
                            ["C", "London", "Move", "Madrid"],
                            ["D", "Paris", "Support", "B"],
                            ["E", "Dublin", "Support", "D"],
                            ["F", "Dallas", "Move", "Paris"]])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]",
                             "C": "[dead]",
                             "D": "Paris",
                             "E": "Dublin",
                             "F": "[dead]"})

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"],
                            ["B", "Barcelona", "Move", "London"],
                            ["C", "London", "Move", "Paris"],
                            ["D", "Paris", "Move", "Madrid"]])
        self.assertEqual(v, {"A": "Barcelona",
                             "B": "London",
                             "C": "Paris",
                             "D": "Madrid"})

    def test_eval4(self):
        v = diplomacy_eval([["A", "Madrid", "Support", "B"],
                            ["B", "Barcelona", "Support", "A"],
                            ["C", "London", "Move", "Madrid"]])
        self.assertEqual(v, {"A": "Madrid",
                             "B": "Barcelona",
                             "C": "[dead]"})

    def test_eval5(self):
        v = diplomacy_eval([["A", "Madrid", "Support", "B"],
                            ["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]"})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        v = {"A": "[dead]",
             "B": "Madrid",
             "C": "London"}
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_2(self):
        w = StringIO()
        v = {"A": "[dead]",
             "B": "[dead]",
             "C": "[dead]",
             "D": "[dead]"}
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_3(self):
        w = StringIO()
        v = {"A": "[dead]",
             "B": "[dead]",
             "C": "[dead]",
             "D": "Paris",
             "E": "Austin"}
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
                     "D Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
..............
----------------------------------------------------------------------
Ran 14 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          69      0     42      0   100%
TestDiplomacy.py      63      0      0      0   100%
--------------------------------------------------------------
TOTAL                132      0     42      0   100%
"""
